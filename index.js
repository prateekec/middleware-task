const express = require('express');
const path =require('path');
const app=express();

app.use(require(path.join(__dirname,'middleware')));

app.get('/logs',(req,res) => {
    res.sendFile(path.join(__dirname,'logfile.txt'));
});
app.get('/:filename',(req,res) =>{
    res.send("Logged to file: "+req.params.filename);
})
app.post('/:filename',(req,res) => {
    res.send("Logged to file :" + req.params.filename);
});

app.put('/:filename',(req,res) => {
    res.send("Logged to file :" + req.params.filename);
});

app.delete('/:filename',(req,res) => {
    res.send("Logged to file :" + req.params.filename);
});


app.listen(8080,'127.0.0.3');
