const fs =require('fs');
const express=require('express');
const path =require('path');

function storeInFile (req,res,next){
    let data=req.method+':'+req.url;
    fs.appendFileSync(path.join(__dirname,'logfile.txt'),data+'\n');
    next();

}
module.exports=storeInFile;